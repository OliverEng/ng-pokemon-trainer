import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/models/trainer.model';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';

@Component({
    selector: 'app-login-form',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.css'],
})
export class LoginFormComponent implements OnInit {
    // define output to pass to parent page
    @Output() login: EventEmitter<void> = new EventEmitter();

    constructor(private readonly _loginService: LoginService, private readonly _userService: UserService) {}

    ngOnInit(): void {}

    public loginSubmit(loginForm: NgForm): void {
        const { username } = loginForm.value;

        // call the login function and emit output to parent login.page function (login)="handleLogin()
        this._loginService.login(username).subscribe({
            next: (user: User) => {
                this._userService.user = user;
                this.login.emit();
            },
            error: () => {
                console.log('Error condition at _loginService.login.subscribe');
            },
        });
    }
}
