import { Component, Input, OnInit } from '@angular/core';
import { PokemonInfo } from 'src/app/models/pokemon-results.model';

@Component({
    selector: 'app-pokemon-list',
    templateUrl: './pokemon-list.component.html',
    styleUrls: ['./pokemon-list.component.css'],
})
export class PokemonListComponent implements OnInit {
    private _searchString = '';
    public sorted: boolean = false;

    @Input() sortedPokemon: PokemonInfo[] = [];
    @Input() pokemons: PokemonInfo[] = [];

    constructor() {}

    ngOnInit(): void {}

    // filter pokemons by search string
    onKey(event: KeyboardEvent) {
        this._searchString = (event.target as HTMLInputElement).value;
        this.sorted = this._searchString.length > 0 ? true : false;

        this.sortedPokemon = this.pokemons.filter((pokemon) => {
            return pokemon.name.includes(this._searchString.toLowerCase());
        });

        console.log(this.sortedPokemon);
    }
}
