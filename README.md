# NG-POKEMON-TRAINER

A web application built on Angular, to showcase competences surrounding TypeScript, HTML, TailwindCSS and Web Frameworks, in connection with Accelerated Learning @ Experis Academy / Noroff.no

## Install

```
git clone https://gitlab.com/OliverEng/ng-pokemon-trainer.git
cd ng-pokemon-trainer
npm install
```

## Extra setup

Create a folder called in environments in the src folder of the project. Create two files as shown here:

### environment.ts
```
    export const environment = {
        production: false,
        apiTrainer: 'APIURL_HERE',
        apiTrainerKey: 'APIKEY_HERE',
        apiPoke: 'https://pokeapi.co/api/v2/',
        apiPokeSpec: 'https://pokeapi.co/api/v2/pokemon/',
    };
```
### environment-prod.ts
```
    export const environment = {
        production: true,
        apiTrainer: 'APIURL_HERE',
        apiTrainerKey: 'APIKEY_HERE',
        apiPoke: 'https://pokeapi.co/api/v2/',
        apiPokeSpec: 'https://pokeapi.co/api/v2/pokemon/',
    };
```

Then replace the "APIKEY/APIURL_HERE" with their relevant pieces, from the creators of the project.

## Usage

The following command will local-host the project at `localhost:4200`. Navigate to 'http://localhost:4200' in your browser.
It may be at a different port, mind you.

```
ng serve
```

The following will build the project. The build artifacts will be stored in the `dist/` directory.

```
ng build
```

Use the following to run unit tests:

```
ng test
```

## Contributors

-   [Oliver Engermann (@OliverEng)](@OliverEng)
-   [Morten Bay Nielsen (@morten.bay)](@morten.bay)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

Noroff Accelerate, 2022.
