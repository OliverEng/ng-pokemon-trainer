import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ImageURLs, StorageKeys } from '../enums/storage-keys.enum';
import { PokemonInfo, PokemonResults } from '../models/pokemon-results.model';
import { StorageUtil } from '../utils/storage.util';
const { apiPoke } = environment;

@Injectable({
    providedIn: 'root',
})
export class PokemonCatalogueService {
    private _pokemon: PokemonInfo[] = [];
    private _error: string = '';
    private _loading: boolean = false;

    get pokemon(): PokemonInfo[] {
        return this._pokemon;
    }

    get error(): string {
        return this._error;
    }

    get loading(): boolean {
        return this._loading;
    }

    constructor(private readonly http: HttpClient) {}

    // get the name and URL for all pokemon
    public getAllPokemon(): void {
        // check if data already exists in storage..
        if (StorageUtil.sessionStorageRead<PokemonInfo[]>(StorageKeys.PokemonList)) {
            // data already exists return data.
            this._pokemon = StorageUtil.sessionStorageRead<PokemonInfo[]>(StorageKeys.PokemonList)!;
            return;
        }

        this._loading = true;
        // get all pokemon from 1-898 since 899 -> are special types with no image data
        this.http.get<PokemonResults>(apiPoke + 'pokemon?limit=898&offset=0.').subscribe({
            next: (pokemonResults: PokemonResults) => {
                // add additional ID and image information
                pokemonResults.results.forEach((result, iteration) => {
                    result.id = iteration + 1;
                    result.image = ImageURLs.OfficialArtwork + (iteration + 1) + '.png';
                });

                // save to session storage and pokemon state
                StorageUtil.sessionStorageSave(StorageKeys.PokemonList, pokemonResults.results);
                this._pokemon = pokemonResults.results;
            },
            error: (error: HttpErrorResponse) => {
                this._error = error.message;
            },
            complete: () => {
                this._loading = false;
            },
        });
    }

    // get pokemon info from session storage by name
    public pokemonCardByName(name: string): PokemonInfo | undefined {
        return this._pokemon.find((pokemon: PokemonInfo) => pokemon.name === name);
    }
}
