import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/trainer.model';

const { apiTrainer, apiTrainerKey } = environment;

@Injectable({
    providedIn: 'root',
})
export class LoginService {
    constructor(private readonly http: HttpClient) {}

    // check if user exists in api, if not create user in api; otherwise return existing user
    public login(username: string): Observable<User> {
        return this.checkUsername(username).pipe(
            switchMap((user: User | undefined) => {
                if (user === undefined) {
                    return this.createUser(username);
                }
                return of(user);
            })
        );
    }

    // check if user already exists in api...
    public checkUsername(username: string): Observable<User | undefined> {
        return this.http.get<User[]>(`${apiTrainer}?username=${username}`).pipe(map((response: User[]) => response.pop()));
    }
    // ...if not, create user in api
    private createUser(username: string): Observable<User> {
        const user = {
            id: 0,
            username,
            pokemon: [],
        };

        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'x-api-key': apiTrainerKey,
        });

        return this.http.post<User>(apiTrainer, user, {
            headers,
        });
    }
}
