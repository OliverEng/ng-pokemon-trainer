import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/trainer.model';
import { PokemonCollectionService } from 'src/app/services/pokemon-collection.service';
import { UserService } from 'src/app/services/user.service';

@Component({
    selector: 'app-catch-button',
    templateUrl: './catch-button.component.html',
    styleUrls: ['./catch-button.component.css'],
})
export class CatchButtonComponent implements OnInit {
    public loading: boolean = false;
    public isCaught: boolean = false;
    @Input() pokemonName: string = '';

    constructor(private readonly userService: UserService, private readonly collectionService: PokemonCollectionService) {}

    ngOnInit(): void {
        // check if pokemon is in collection
        this.isCaught = this.userService.inCollection(this.pokemonName);
    }

    onCatchClick(): void {
        this.loading = true;
        // add pokemon to collection
        this.collectionService.addToCollection(this.pokemonName).subscribe({
            next: (user: User) => {
                this.loading = false;
                // check if pokemon is in collection
                this.isCaught = this.userService.inCollection(this.pokemonName);
            },
            error: (error: HttpErrorResponse) => {
                console.log(error.message);
            },
        });
    }
}
