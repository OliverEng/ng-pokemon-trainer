import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/trainer.model';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { PokemonInfo } from 'src/app/models/pokemon-results.model';

@Component({
    selector: 'app-trainer',
    templateUrl: './trainer.page.html',
    styleUrls: ['./trainer.page.css'],
})
export class TrainerPage implements OnInit {
    private _pokemonList: PokemonInfo[] = [];

    get user(): User | undefined {
        return this._userService.user;
    }

    // get list of pokemon objects from catalogue service, from pokemon names saved in user.
    get pokemonList(): PokemonInfo[] {
        if (this._userService.user) {
            this._pokemonList = [];
            this.user?.pokemon.forEach((pokemon) => {
                this._pokemonList.push(this._catalogueService.pokemonCardByName(pokemon)!);
            });
            return this._pokemonList;
        }
        return [];
    }
    constructor(
        private readonly _router: Router,
        private readonly _userService: UserService,
        private readonly _catalogueService: PokemonCatalogueService
    ) {}

    ngOnInit(): void {
        this._catalogueService.getAllPokemon(); // get all pokemon from api or session storage
    }

    handleLogout(): void {
        this._userService.user = undefined; // delete local session for the invalid user
        this._router.navigateByUrl('/login'); // redirect to logon page
    }
}
