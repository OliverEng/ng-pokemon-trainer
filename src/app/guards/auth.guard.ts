import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';
import { UserService } from '../services/user.service';
import { User } from 'src/app/models/trainer.model';

@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivate {
    constructor(
        private readonly _router: Router,
        private readonly _userService: UserService,
        private readonly _loginService: LoginService
    ) {}

    canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        // check if a user exists in local session storage.
        if (this._userService.user) {
            let userToCheck = this._userService.user.username;

            // check if the user exists in the API
            this._loginService.checkUsername(userToCheck).subscribe({
                next: (user: User | undefined) => {
                    if (user === undefined) {
                        console.log(`Auth: user "${userToCheck}" does not exist in API`);
                        this._userService.user = undefined; // delete local session for the invalid user
                        this._router.navigateByUrl('/login'); // redirect to logon page
                    }
                },
                error: () => {
                    // error condition, so we must invalidate the stored login session and require user to login again.
                    this._userService.user = undefined; // delete local session for the invalid user
                    this._router.navigateByUrl('/login'); // redirect to logon page
                    console.log('Auth: Error condition at _loginService.checkUsername');
                },
            });

            return true;
        } else {
            console.log('Auth: no local storage user exists');
            this._router.navigateByUrl('/login');
            return false;
        }
    }
}
