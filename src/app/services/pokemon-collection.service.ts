import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/trainer.model';
import { PokemonInfo } from '../models/pokemon-results.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';

const { apiTrainer, apiTrainerKey } = environment;

@Injectable({
    providedIn: 'root',
})
export class PokemonCollectionService {
    private _pokemonList: PokemonInfo[] = [];

    get user(): User | undefined {
        return this._userService.user;
    }

    // get a list of pokemon info in session storage, by name
    get pokemonList(): PokemonInfo[] {
        if (this._userService.user) {
            this._pokemonList = [];
            this.user?.pokemon.forEach((pokemon) => {
                this._pokemonList.push(this._catalogueService.pokemonCardByName(pokemon)!);
            });
            return this._pokemonList;
        }
        return [];
    }

    constructor(
        private readonly http: HttpClient,
        private readonly _userService: UserService,
        private readonly _catalogueService: PokemonCatalogueService
    ) {}

    // add or remove pokemon to collection
    public addToCollection(pokemonName: string): Observable<User> {
        if (!this._userService.user) {
            throw new Error('addToCollection: There is no user');
        }

        // get pokemon object from pokemon name
        const pokemon: PokemonInfo | undefined = this._catalogueService.pokemonCardByName(pokemonName);

        // check if pokemon could be found in pokemon info list
        if (!pokemon) {
            throw new Error('add To Collection: No pokemon with name: ' + pokemonName);
        }

        const user: User = this._userService.user;

        if (this._userService.inCollection(pokemonName)) {
            this._userService.removeFromCollection(pokemonName);
        } else {
            this._userService.addToCollection(pokemonName);
        }

        const body = {
            pokemon: [...user.pokemon], // copy array
        };

        // define http headers
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'x-api-key': apiTrainerKey,
        });

        // define the patch request
        return this.http
            .patch<User>(`${apiTrainer}/${this._userService.user?.id}`, body, {
                headers,
            })
            .pipe(
                tap((updatedUser: User) => {
                    this._userService.user = updatedUser; // Update the user object
                })
            );
    }
}
