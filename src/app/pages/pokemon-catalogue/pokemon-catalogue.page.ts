import { Component, OnInit } from '@angular/core';
import { PokemonInfo } from 'src/app/models/pokemon-results.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
    selector: 'app-pokemon-catalogue',
    templateUrl: './pokemon-catalogue.page.html',
    styleUrls: ['./pokemon-catalogue.page.css'],
})
export class PokemonCataloguePage implements OnInit {
    get pokemons(): PokemonInfo[] {
        return this._catalogueService.pokemon;
    }

    get loading(): Boolean {
        return this._catalogueService.loading;
    }

    get error(): string {
        return this._catalogueService.error;
    }

    constructor(private readonly _catalogueService: PokemonCatalogueService) {}

    ngOnInit(): void {
        this._catalogueService.getAllPokemon(); // get all pokemon from api or session storage
    }
}
