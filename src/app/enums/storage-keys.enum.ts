export enum StorageKeys {
    User = 'trainer-user',
    PokemonList = 'pokemon-list',
    PokemonListAllInfo = 'pokemon-all-info-list',
}

export enum ImageURLs {
    OfficialArtwork = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/',
}
