import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { User } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
    providedIn: 'root',
})
export class UserService {
    private _user?: User;

    get user(): User | undefined {
        this._user = StorageUtil.localStorageRead<User>(StorageKeys.User);
        return this._user;
    }

    set user(user: User | undefined) {
        StorageUtil.localStorageSave<User>(StorageKeys.User, user!);
        this._user = user;
    }

    constructor() {
        this._user = StorageUtil.localStorageRead<User>(StorageKeys.User);
    }

    public inCollection(pokemon: string): boolean {
        if (this._user) {
            return Boolean(this._user?.pokemon.find((pokemonName: string) => pokemon === pokemonName));
        }
        return false;
    }
    public addToCollection(pokemon: string): void {
        if (this._user) {
            this._user.pokemon.push(pokemon);
        }
    }

    public removeFromCollection(pokemon: string): void {
        if (this._user) {
            this._user.pokemon = this._user.pokemon.filter((pokemonName: string) => pokemon !== pokemonName);
        }
    }
}
